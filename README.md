# README #

Need a quick way to send a message to slack from the command line? This will do the job (and not much else)

Useful inside scripts to report back to an administrator.

### What is this repository for? ###

* Shell script that uses curl to send a message to slack
* Very simple
* By [Hansen IT Solutions](https://hansenit.solutions)

### How do I get set up? ###

* Summary of set up
	* Install curl to your system.
	* Copy the slackecho script to your binary path
* Configuration
	* Update the SLACKURL with your own (todo: create a config file and a way to setup the file on first run)
	* Update the 'defaults' within the script
* Dependencies
	* curl
* How to run tests
	* N/A

### Contribution guidelines ###

###TODO###
* Writing tests
* Code review
* Other guidelines
* Configuration moved to a seperate file

### Who do I talk to? ###

* gary@hansenit.solutions